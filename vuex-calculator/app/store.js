import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    message: "Hello world"
};

const mutations = {

};

export default new Vuex.Store({
    state,
    mutations
})