import Vue from 'vue/dist/vue.esm.js'
import AppComponent from  './app.component';
import Vuex from 'vuex';
import store from './store';

Vue.use(Vuex)
new Vue({
    el: "#app",
    data: {
        message: 'Hello World'
    },
    components: {
        'app-component': AppComponent
    },
    store
});