export default {
    props:['inline'],
    template: require('html-loader!./result.component.html'),
    vuex: {
        getters : {
            myMessage: function(state){
                return state.message
            }    
        }
    }
}