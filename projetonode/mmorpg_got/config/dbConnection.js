var assert = require("assert");
const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost:27017';  // mongodb://localhost - will fail
const dbName = "got";

/* var connMongoDB = function(dados) {
    mongo.connect(url, function(err, client) {
        assert.equal(null, err);
        console.log("Connected successfully to server");
        const db = client.db(dbName);
        query(db, dados);
        client.close();
    });
};
 */

async function connMongoDB(dados) {
  try {
    const client = await MongoClient.connect(uri,{ useNewUrlParser: true });
    console.log("Connected successfully to server");
    const db = client.db(dbName);
    query(db, dados);
    client.close();
  } catch(e) {
    console.error(e)
  }

}

function query(db, dados) {
    var collection = db.collection(dados.collection);

    switch (dados.operacao) {
        case "inserir":
            collection.insertOne(dados.usuario, dados.callback);
            break;
        case "autenticar":
            const { usuario, callback } = dados;
            collection.find(usuario,callback);
            break;    
        default:
            break;
    }
}

module.exports = function() {
    return connMongoDB;
};