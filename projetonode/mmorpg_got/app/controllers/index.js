module.exports.index = function (application,req,res){
    res.render('index', {validacao:{}});
}

module.exports.autenticar = function(application,req,res){
  let dadosForm = req.body;

  req.assert('usuario','Usuário não deve ser vazio').notEmpty();
  req.assert('senha','Senha não deve ser vazio').notEmpty();

  var erros = req.validationErrors();

  if(erros){
      res.render('index',{validacao: erros});
      return;
  }

  let connection = application.config.dbConnection;  
  let UsuariosDAO = new application.app.models.UsuariosDAO(connection);
  UsuariosDAO.autenticar(dadosForm,req,res);

  
}
