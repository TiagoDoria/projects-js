import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Forms from '@/components/Forms'
import ListUser from '@/components/ListUser'
import EditUser from '@/components/EditUser'
import Login from '@/components/Login'
import HomeUser from '@/components/HomeUser'
import Alura from '@/components/Alura'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/forms',
      name: 'Forms',
      component: Forms
    },
    {
      path: '/forms/:id',
      name: 'EditUser',
      component: EditUser
    },
    {
      path: '/users',
      name: 'ListUser',
      component: ListUser
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/homeuser',
      name: 'HomeUser',
      component: HomeUser
    },
    {
      path: '/alura',
      name: 'Alura',
      component: Alura
    }
  ]
})
