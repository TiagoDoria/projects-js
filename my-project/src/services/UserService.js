
import Api from '@/services/Api'

export default {
  fetchUsers () {
    return Api().get('users')
  },
  addUser (params) {
    return Api().post('users', params)
  },

  updateUser (params) {
    return Api().put('users/' + params.id, params)
  },

  getUser (params) {
    return Api().get('users/' + params.id)
  },
  deleteUser (id) {
    return Api().delete('users/' + id)
  },

  filterUser (sname) {
    return Api().get('users/filter/' + sname)
  },
  loginUser (params) {
    return Api().post('/login', params)
  },

  logouts () {
    return Api().get('/logout')
  }
}