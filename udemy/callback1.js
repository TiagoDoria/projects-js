const notas = [7.7, 6.5, 5.2, 8.9, 3.6, 8.9, 7.1, 9.0]

const notasBaixas = notas.filter(function(nota){
    return nota < 7;
})

console.log(notasBaixas);

const notaBaixas2 = notas.filter(nota => nota < 7);

console.log(notasBaixas);