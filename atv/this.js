var father = {
    name: "Antonio",
    child: {
        name: "Tiago",
        func: function(){
            return this.name;
        }
    }
}

console.log(father.child.func());
//this se refere ao pai mais proximo