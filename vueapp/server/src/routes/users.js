
module.exports = function (app) {

    var User = require("../../models/user");
    var bcrypt = require("bcrypt");
    var session = require('express-session');
    var passport = require("passport");
    const jwt = require('jsonwebtoken');

      app.use(session({
        secret: 'iuashddiugfhhskadnf',
        resave: false,
        saveUninitialized: false
      }));
    

      app.get('/users', (req, res) => {
        User.find({}, 'name email state gender', function (error, users) {
            if (error) { console.error(error); }
            res.send({
              users: users
            })
          }).sort({_id:-1})
      })
    
    
      app.post('/users', (req, res) => {
        var db = req.db;
        var name = req.body.name;
        var email = req.body.email;
        var hash = bcrypt.hashSync(req.body.password,10);
        var state = req.body.state;
        var gender = req.body.gender;
        var new_user = new User({       
          name: name,
          email: email,
          password: hash,
          state: state,
          gender: gender
        })
      
        new_user.save(function (error) {
          if (error) {
            console.log(error)
          }
          res.send({
            success: true,
            message: 'User saved successfully!'
          })
        })
      })
    
      app.get('/users/:id', (req, res) => {
        var db = req.db;
        User.findById(req.params.id, 'name email state gender', function (error, user) {
          if (error) { console.error(error); }
          res.send(user)
        })
      })

      app.get('/users/filter/:sname', (req,res) => {
          var db = req.db;
       
          User.find({name:{$regex: req.params.sname}}, 'name email state gender', function (error, users){
              if(error){  console.error(error); }
                res.send({
                  users: users
              })
          })
          
      })
      
      app.put('/users/:id', (req, res) => {
        var db = req.db;
        User.findById(req.params.id, ' name email state gender', function (error, user) {
          if (error) { console.error(error); }
      
          user.name = req.body.name
          user.email = req.body.email
          user.password = req.body.password
          user.state = req.body.state
          user.gender = req.body.gender
          user.save(function (error) {
            if (error) {
              console.log(error)
            }
            res.send({
              success: true
            })
          })
        })
      })
    
      app.delete('/users/:id', (req, res) => {
        var db = req.db;
        User.remove({
          _id: req.params.id
        }, function(err, user){
          if (err)
            res.send(err)
          res.send({
            success: true
          })
        })
      })    


      app.post('/login', (req, res) => {
       
        User.findOne({name: req.body.username})
        .exec()
        .then(function(user){
          bcrypt.compare(req.body.password,user.password, function(err,result){
           
            if(err){
              return res.status(401).send({
                auth: false,
                failed: 'Unauthorized Access'
              });
            }
            if(result){
              req.session.saveUninitialized = true;
              
              return res.status(200).send({
                auth: true,
                success: 'Welcome to the JWT Auth' ,   
              });
             
            }
            return res.status(401).send({
              auth: false,
              failed: 'Unauthorized Access'
            });
          });
        })
        .catch(error => {
          res.status(500).json({
            error: error
          });
        });
      });


      app.get('/logout', function(req, res) {
        res.status(200).send({ auth: false, token: null });
      });

    
}
