// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue/dist/vue.esm.js'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
import router from './router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
import VeeValidate from 'vee-validate'
import VueSession from 'vue-session'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import './directives/transform'

Vue.config.productionTip = false

Vue.use(VueMaterial)
Vue.use(ElementUI, { size: 'small' })
Vue.use(VueResource)
Vue.use(VueSession)
Vue.use(VueRouter)
Vue.use(VeeValidate)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  render: h => h(App),
  template: '<App/>'
})
