import Vue from 'vue'

Vue.directive('meu-transform', {
    bind(el, binding,vnode){
        let current = 0;
        el.addEventListener('dblclick', function(){
            let inc = binding.value || 90;
            current+=inc;
            el.style.transition = 'transform 0.5';
            el.style.transform = `rotate(${current}deg)`;
        });
    }
});